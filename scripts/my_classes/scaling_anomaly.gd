class_name ScalingAnomaly
extends Anomaly

var is_scaling : bool = false
var original_scales : Array = []

func _ready():
	super()

	only_activate_offscreen = false
	only_activate_onscreen = false

	anomaly_name = "ScalingAnomaly"

	for child in children_to_operate_on_activate:
		print("setting original scale " + str(child.scale))
		original_scales.append(child.scale)

func _process(delta):
	if is_scaling:
		for child in children_to_operate_on_activate:
			child.scale = Vector2(child.scale.x + .03 * delta, child.scale.y + .03 * delta)
			
		
func activate():
	super()
	is_scaling = true

func deactivate():
	super()
	is_scaling = false
	
	var index = 0
	for child in children_to_operate_on_activate:
		print("Resetting Anomly ScalingAnomaly to original scale " + str(child.scale))
		child.scale = original_scales[index]
		index += 1
