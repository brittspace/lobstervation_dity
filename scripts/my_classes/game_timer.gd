class_name GameTimer
extends Node


var total_game_time = 1800  # Total game time in seconds (30 minutes)
var in_game_time = 0  # Tracks in-game time from 0 to 21600 seconds (6 hours)
var real_life_time = 0
var one_hour = 3600  # 60 minutes * 60 seconds = 3600

# Since the game lasts 30 real-world minutes but represents 6 hours in-game,
# the time acceleration factor would be 12x (6 hours / 30 minutes).

# To adjust the game so that 30 real-life minutes equate to 6 in-game hours, 
# we'll need a time acceleration factor. This can be calculated as 
# (6 hours × 60 minutes/hour) / 30 minutes
# = 12. This means every real second is equivalent to 12 seconds of in-game time.
var time_scale = 12  # Factor to convert real seconds to in-game seconds

@onready var anomaly_timer = $AnomalyTimer
var current_interval = 0

signal in_game_hour_passed

func _ready():
	set_process(false)
	pass

func start_game():
	set_process(true)
	schedule_next_anomaly()

func _process(delta):
	in_game_time += delta * time_scale
	real_life_time += delta
	if in_game_time >= one_hour:
		emit_signal("in_game_hour_passed", in_game_time / one_hour)

		# this is going to loop hours?
		#in_game_time -= in_game_hour
	
	#$/root/Game/RoomUI.set_game_time(in_game_time)
	#$/root/Game/RoomUI.set_real_life_time(real_life_time)

func schedule_next_anomaly():
	#current_interval = randf_range(120, 180)  # Random time between 2 and 3 minutes of real time
	current_interval = randf_range(5, 10)  # Random time between 2 and 3 minutes of real time
	print("scheduling an anomaly for " + str(current_interval))
	anomaly_timer.start(current_interval)

func _on_anomaly_timer_timeout():
	GameManager.activate_random_anomaly()
	print("anomaly activated on timer!")
	#adjust_timing()  # Optional: Adjust timing as the game progresses
	schedule_next_anomaly()

# we can adjust timing by changing the rand based on time left and their number of misses
#func adjust_timing():
	## Optionally adjust the timing to make anomalies happen more frequently towards the end
	#if total_game_time - anomaly_timer.time_left < 600:  # Last 10 minutes
	#current_interval = max(current_interval - 10, 60)  # Minimum 1 minute
