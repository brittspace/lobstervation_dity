extends Resource
class_name SoundEffect

@export var tag: String
@export var stream: AudioStream 
@export var volume: float = 0
