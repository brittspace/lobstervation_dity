class_name Anomaly
extends Node

var is_active = false
@export var only_activate_offscreen : bool = false  # true if anomaly should activate only when room is offscreen
@export var only_activate_onscreen : bool = false  # true if anomaly should activate only when room is onscreen
@export var anomaly_name: String

signal anomaly_activated
signal anomaly_solved

# Hard coding the path here doesn't feel great, but it's the best way I've found to get the room *shrug*
@onready var room : Room = $"../.."

var children_to_operate_on_activate: Array = []

func _ready():
	GameManager.anomalies.append(self)

	print("Anomaly " + name + " of type " + anomaly_name + " ready " + "in room " + room.name)

	# get all children that we should operate on when activating
	for child in get_children():
		if child is Sprite2D:
			children_to_operate_on_activate.append(child)

# This is the main function that should be called to determine if the anomaly should be activated
func is_eligable_for_activate_in_room():
	print("checking room eligability for anomaly " + name + " of type " + anomaly_name)
	if is_eligable_for_activation():

		print("Anomaly " + name + " room.is_on_screen: " + str(room.is_on_screen()) + " -- only_activate_offscreen: " + str(only_activate_offscreen) + " -- only_activate_onscreen: " + str(only_activate_onscreen) + " -- is_active: " + str(is_active))
		if only_activate_offscreen and !room.is_on_screen():
			return true
		if only_activate_onscreen and room.is_on_screen():
			return true
		if !only_activate_offscreen and !only_activate_onscreen:
			return true
		return false


func is_eligable_for_activation():
	if !is_active and (self not in GameManager.solved_anomalies):
		print("Anomaly " + name + " of type " + anomaly_name + " is active nad not in solved")
		return true
	return false

# There are no guardrails on whether they can be activated here.
# That's done in outside logic
func activate():
	print("Activating anomaly " + name + " of type " + anomaly_name)
	GameManager.active_anomalies.append(self)
	emit_signal("anomaly_activated")
	is_active = true

# There are no guardrails on whether they can be deactivated here.
# That's done in outside logic
func deactivate():
	print("Deactivating anomaly " + name + " of type " + anomaly_name)
	is_active = false

	# store anomaly in an array that indicates it's been solved and shouldn't be solved again
	GameManager.active_anomalies.erase(self)

	# Remove from the active anomalies array in Game Manager
	GameManager.solved_anomalies.append(self)	
	emit_signal("anomaly_solved")

