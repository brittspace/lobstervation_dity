class_name DisappearingAnomaly
extends Anomaly

@export var image_add : bool = false

func _ready():
	super()

	only_activate_offscreen = true
	only_activate_onscreen = false

	anomaly_name = "DisappearingAnomaly"

# There are no guardrails on whether they can be activated here.
# That's done in outside logic
func activate():
	super()
	for child in children_to_operate_on_activate:
			child.visible = image_add

# There are no guardrails on whether they can be deactivated here.
# That's done in outside logic
func deactivate():
	super()
	for child in children_to_operate_on_activate:
		child.visible = !image_add
