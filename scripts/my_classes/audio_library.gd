extends Resource
class_name AudioLibrary

@export var sound_effects: Array[SoundEffect]

func get_audio_stream(_tag: String):
	if _tag:
		var i = 0

		for sound in sound_effects:
			if sound.tag == _tag:
				break
			i += 1

		if i < sound_effects.size():
			return sound_effects[i]
		else:
			printerr("couldn't find a sound with that tag")
	else:
		printerr("no tag provided, cannot get sound effect")
	return null
