class_name Room
extends Node

@onready var anomalies_node : Node = get_node("Anomalies")
var anomalies : Array = []

func _ready():
	#GameManager.rooms.append(self)
	anomalies = _get_anomalies()

	#get_parent().remove_child(self)
	print("Room " + self.name + " node is loaded")
	

func _get_anomalies():
	if anomalies_node:
		return anomalies_node.get_children()
	else:
		return []
	
func _exit_tree():
	if $BackgroundAudio:
		$BackgroundAudio.stop()

func is_on_screen():
	if GameManager.current_room == self:
		print_debug("is current room: " + GameManager.current_room.name + " vs " + self.name)
		return true
	else:
		print_debug("not current room: " + GameManager.current_room.name + " vs " + self.name)
		return false

func activate_all_anomaly():
	for anomaly : Anomaly in anomalies:
		if anomaly.is_eligable_for_activation():
			anomaly.activate()

func deactivate_all_anomalies():
	for anomaly in anomalies:
		if anomaly.is_active:
			anomaly.deactivate()
		
func activate_random_anomaly():
	var eligible_anomalies = []
	if anomalies_node:
		for anomaly in anomalies:
			if anomaly.is_eligable_for_activation():
				eligible_anomalies.append(anomaly)
	
	# Check if there are any eligible anomalies to activate
	if eligible_anomalies.size() > 0:
		var anomaly_to_activate = eligible_anomalies[randi() % eligible_anomalies.size()]
		anomaly_to_activate.activate()  # Activate the selected anomaly
		print_debug("Activated anomaly " + anomaly_to_activate.name + " in " + name)
		return true  # Return true if an anomaly was activated
	else:
		return false  # Return false if no anomalies were eligible

func check_if_an_anomaly_type_is_active(anomaly_type : String):
	var eligible_anomalies = []
	for anomaly in anomalies:
		if anomaly is Anomaly and anomaly.is_active and anomaly.anomaly_name == anomaly_type:
			print("found an anom!" + anomaly.anomaly_name + anomaly_type)
			return anomaly
		else:
			print("skipping anom " + anomaly.anomaly_name + anomaly.name)
	return null
	
