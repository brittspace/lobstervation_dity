extends Control

var currently_selected_anomaly : String
var currently_selected_room : String
#@onready var anomaly_grid_container = $AnomalySelectionContainer/VBoxContainer/AnomalyGridContainer
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
	#call_deferred("_setup_connections")
	#print("first wtf" + str(anomaly_grid_container))
	#var anomaly_grid_container = get_node("AnomalySelectionContainer/VBoxContainer/AnomalyGridContainer")
	#var anomaly_grid_container = "AnomalySelectionContainer")
	# Hook up the anomaly selection checkboxes
	
	#for anomaly in anomaly_grid_container.get_children():
		#var checkbox = anomaly.get_node("CheckBox")
		#checkbox.connect("_anomaly_checkbox_toggled", _on_anomaly_checkbox_toggled)

#func _setup_connections():
	##var anomaly_grid_container = get_node("AnomalySelectionContainer/VBoxContainer/AnomalyGridContainer")
	#print("wtf " + str(anomaly_grid_container))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _toggle_anom_selection_container():
	PolyphonicAudioPlayer.play_sound_effect_from_library("schwip")
	var container_visible = $AnomalySelectionContainer.visible
	$AnomalySelectionContainer.visible = !container_visible
	$ReportAnomalyButton.visible = container_visible

func _on_report_anom_button_button_up():
	_toggle_anom_selection_container()


func _on_send_button_button_up():
	print("Selected Room " + currently_selected_room)
	print("Selected Anomaly " + currently_selected_anomaly)
	GameManager.check_for_anomaly_in_room(currently_selected_room, currently_selected_anomaly)
	_toggle_anom_selection_container()


func _on_cancel_button_button_up():
	_toggle_anom_selection_container()

func _on_anomaly_checkbox_toggled(toggled_on, anomaly_name):
	if toggled_on == true:
		print("got a anomaly true signal for " + anomaly_name)	
		currently_selected_anomaly = anomaly_name

func _on_room_checkbox_toggled(toggled_on, anomaly_name):
	print("toggled: " + str(toggled_on) + " " + anomaly_name)
	if toggled_on == true:
		print("got a room true signal for " + anomaly_name)	
		currently_selected_room = anomaly_name
