# There's a real argument that this should be a child node of Game
# and that the main menu should be a separate scene that changes scenes
# to Game

# I think that this would especially make sense if we had a different level

extends Node

# dict of the rooms
var rooms : Array = [] 

# manage anomalies
var anomalies : Array = []
var solved_anomalies : Array = []
var active_anomalies : Array = []

var current_room : Room = null
var number_of_active_anomalies = 0

# dev toggle for whether anomalies should trigger through the night
var enable_timed_anoms : bool = false

# convenience variables
@onready var game_timer : GameTimer = $GameTimer

# These would NOT work if we actually changed scenes to swithc between
# main menus and Game, for example, because GameManager is a AutoLoad
# If we change that, we need to either move these into functions where we can
# be sure that they're loaded, or make GameManager a child of Game (makes the most sense)
@onready var game_ui = $/root/Game/RoomUI
@onready var rooms_node = $/root/Game/Rooms

signal game_started

func setup_game():
	print("Setting up game")
	setup_rooms()
	setup_anomalies()
	

func start_game():
	print("Starting game")
	
	# This signal currently updates the UI, but it could be used to trigger other things
	emit_signal("game_started")
	_goto_room_by_name("Livingroom")
	start_timer()
	

func setup_rooms():
	# when following this idea https://docs.godotengine.org/en/4.0/tutorials/scripting/change_scenes_manually.html
	# I'm currently doing #3, but if I don't have them loaded at first via the editor and then removed here
	# then I can't do some things like operate on nodes in a room that hasn't been visited yet
	# I still think there's a chance that I should implement a "hide" method on each room and just use that to 
	# keep them in memory and processing in the background, so something like a scaling node will continue to get bigger

	for room in rooms_node.get_children():
		if room is Room:
			print("Setting up room: " + room.name)
			rooms.append(room)
			rooms_node.remove_child(room)

	game_ui.create_room_selects()

	print("Room array set up: " + str(rooms))

func setup_anomalies():
	for room in rooms:
		for anomaly in room.anomalies:
			anomaly.connect("anomaly_activated", _on_anomaly_activated)	
			anomaly.connect("anomaly_solved", _on_anomaly_solved)	

func _on_anomaly_activated():
	number_of_active_anomalies += 1
	_update_debug_anomaly_labels()
	print("Adding to anomaly count")

func _on_anomaly_solved():
	number_of_active_anomalies -= 1
	_update_debug_anomaly_labels()
	print("Subtracting to anomaly count")
	
# this is a hack and should be a signal
func _update_debug_anomaly_labels():

	# Anomaly Count
	game_ui.find_child("ActiveAnomalyCountLabel", true, false).text = "Active Anomaly Count: " + str(number_of_active_anomalies)

	# Anomaly Names + Rooms
	var anomalies_for_ui = []
	for anomaly in active_anomalies:
		anomalies_for_ui.append(anomaly.name + " in " + anomaly.room.name + " (" + anomaly.anomaly_name + ")")

	game_ui.find_child("ActiveAnomaliesLabel", true, false).text = "Active Anomalies: " + str(anomalies_for_ui)

func _get_room_by_name(room_name):
	for room in rooms:
		if room.name == room_name:
			return room
	return null

func _get_room_index(target_room : Room):
	var index = 0
	for room in rooms:
		if room == target_room:
			return index
		index += 1
	
func _goto_room_by_name(room_name):
	var room = _get_room_by_name(room_name)
	_goto_room(room)

func _goto_next_room():
	if !current_room:
		printerr("No current room")
		return false
	
	var current_room_index = _get_room_index(current_room)
	var next_room_index : int
	
	# if we're at the end of the rooms list, go to the first room
	if current_room_index == rooms.size() - 1:
		next_room_index = 0
	else:
		next_room_index = current_room_index + 1

	_goto_room_by_index(next_room_index)

	
func _goto_previous_room():
	if !current_room:
		printerr("No current room")
		return false

	var current_room_index = _get_room_index(current_room)
	var previous_room_index : int
	
	# if we're at the beginning of the rooms list, go to the last room
	if current_room_index == 0:
		previous_room_index = rooms.size() - 1
	else:
		previous_room_index = current_room_index - 1

	_goto_room_by_index(previous_room_index)
	
func _goto_room_by_index(index):
	if index < 0 or index >= rooms.size():
		printerr("Invalid room index: " + str(index))
		return false
	
	var room = rooms[index]
	_goto_room(room)

# This is a bit hacky and should probably be managed by the rooms node?
# And a signal?
func _goto_room(room : Room):

	# if we're already in the room, don't do anything
	if current_room == room:
		printerr("Tried to go to the room we're already in")
		return false
	
	if current_room:
		# remove the current room from the scene tree
		rooms_node.remove_child(current_room)
	
	# add the new room to the scene tree
	rooms_node.add_child(room)
	get_tree().set_current_scene(room)
	current_room = room

	$/root/Game/RoomUI.set_room_name(room.name)

	return true

func next_room():
	_goto_next_room()

func previous_room():
	_goto_previous_room()

# execute a random anomaly, handling the case where some can only be done offscreen and some can only be done onscreen
# returns true if an anomaly was activated, false if no anomalies were eligible
func activate_random_anomaly():
	var eligable_anomalies = []

	# Get all anomalies in the game
	print("Looking for a random anomaly to activate")
	print("all anomalies " + str(anomalies))
	# Filter out anomalies that are currently active, have already been activated, or are not eligible for activation because of the room we're in
	for anomaly : Anomaly in anomalies:	
		if anomaly.is_eligable_for_activate_in_room():
			eligable_anomalies.append(anomaly)

	print("Eligable anomalies: " + str(eligable_anomalies))
	
	# Check if there are any eligible anomalies to activate
	if eligable_anomalies.size() > 0:
		var anomaly_to_activate : Anomaly = eligable_anomalies[randi() % eligable_anomalies.size()]
		anomaly_to_activate.activate()  # Activate the selected anomaly
		return true  # Return true if an anomaly was activated

	print("No anomalies found to activate")
	return false


# Function to activate a random eligible anomaly offscreen from the current room
func activate_anomaly_offscreen():
	var eligible_anomalies = []

	# Filter for rooms not currently viewed
	for room : Room in rooms:
		if room.name != current_room.name:
			print("checking for offscreen anomalies in " + room.name)
			# In each room, look for eligible anomalies
			for anomaly : Anomaly in room.anomalies:
				if anomaly.is_eligable_for_activation():
					eligible_anomalies.append(anomaly)
	
	# Check if there are any eligible anomalies to activate
	if eligible_anomalies.size() > 0:
		var anomaly_to_activate : Anomaly = eligible_anomalies[randi() % eligible_anomalies.size()]
		anomaly_to_activate.activate()  # Activate the selected anomaly
		return true  # Return true if an anomaly was activated
	
	print("No anomalies found to activate in offscreen rooms")
	return false  # Return false if no anomalies were eligible

# Function to hide an anomaly in the current room
func activate_random_anomaly_on_current_room():
	var success = current_room.activate_random_anomaly()
	if success:
		print_debug("Activated a random anomaly on the current room")
		return true
	
	print_debug("No eligable anomalies to hide on the current room")
	return false

# temp func to hide something random in the living room
func activate_anomaly_in_livingroom():
	var livingroom = _get_room_by_name("Livingroom")
	livingroom.activate_random_anomaly()
		
# TODO move to room class
func reset_anomalies_in_room(room : Room = current_room):
	room.deactivate_all_anomalies()

func _on_date_time_display_anom_time():
	pass
	#if enable_timed_anoms: hide_in_livingroom()

# Function that gets called when the player submits a report for an anomaly
func check_for_anomaly_in_room(room_name, anomaly_name):
	var room = _get_room_by_name(room_name)
	if room:
		var anomaly = room.check_if_an_anomaly_type_is_active(anomaly_name)
		if anomaly:
			# they found an anomaly
			# TODO transition to fixing anomaly scene
			PolyphonicAudioPlayer.play_sound_effect_from_library("way_to_go")
			anomaly.deactivate()
			return true

		# they didn't find an anomaly
		PolyphonicAudioPlayer.play_sound_effect_from_library("failure")
		return false
		
func start_timer():
	$GameTimer.start_game()
