extends CanvasLayer

var room_select_scene = preload("res://scenes/room_select.tscn")

func _ready():
	create_room_selects()
	GameManager.connect("game_started", _on_game_started)

func _process(delta):
	if Input.is_action_just_pressed("ShowHideDebug"):
		$Debug.visible = !$Debug.visible
		
func _on_game_started():
	self.show()

# Programatically create room selections
# May be a bit overkill but was fun to do it this way and allows
# For really easy adding of rooms
# Can always do it by hand if needed
func create_room_selects():
	var room_select_button_group = load("res://scenes/room_select_button_group.tres")
	
	for room in GameManager.rooms:
		var rooms_grid = $AnomalySelection/AnomalySelectionContainer/VBoxContainer/VBoxContainer/RoomsGridContainer
		
		var room_select = room_select_scene.instantiate()
		room_select.name = room.name
		room_select.get_node("Label").text = room.name
		
		var checkbox : CheckBox = room_select.get_node("CheckBox")
		checkbox.button_group = room_select_button_group
		checkbox.toggled.connect(Callable($AnomalySelection, "_on_room_checkbox_toggled").bind(room.name))
		
		rooms_grid.add_child(room_select)
	
#func set_game_time(time):
	#$BottomLeftInfo/VBoxContainer/InGameTime.text = "In Game Time: " + str(time)
#
#func set_real_life_time(time):
	#$BottomLeftInfo/VBoxContainer/RealLifeTime.text = "Real Life Time: " + str(time)

# This feels fragile and the node should probably update itself
func set_room_name(room_name : String):
	$BottomLeftInfo/CurrentRoomLabel.text = room_name

func _on_next_room_button_button_up():
	PolyphonicAudioPlayer.play_sound_effect_from_library("boop")
	GameManager.next_room()

func _on_prev_room_button_button_up():
	PolyphonicAudioPlayer.play_sound_effect_from_library("boop_low")
	GameManager.previous_room()

func _on_reset_anoms_in_room_button_button_up():
	PolyphonicAudioPlayer.play_sound_effect_from_library("schwip")
	GameManager.reset_anomalies_in_room()

func _on_date_time_display_anom_time():
	GameManager._on_date_time_display_anom_time()

# Dev button pressed to activate a random offscreen anomaly
func _on_activate_anomaly_offscreen_button_up():
	PolyphonicAudioPlayer.play_sound_effect_from_library("schwip")
	GameManager.activate_anomaly_offscreen()

# Dev button pressed to activate a random anomaly in the current room
func _on_activate_random_anomaly_button_pressed():
	PolyphonicAudioPlayer.play_sound_effect_from_library("schwip")
	GameManager.activate_random_anomaly()

func _on_start_timer_button_pressed():
	GameManager.start_timer()
