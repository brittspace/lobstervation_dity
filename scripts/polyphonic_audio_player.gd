extends AudioStreamPlayer

@export var audio_library: AudioLibrary
@export var custom_max_polyphony: int = 32

# Called when the node enters the scene tree for the first time.
func _ready():
	stream = AudioStreamPolyphonic.new()
#	stream.polyphony = custom_max_polyphony

func play_sound_effect_from_library(_tag: String, custom_volume = null) -> void:
	if _tag:

		var audio_info = audio_library.get_audio_stream(_tag)
		var audio_volume
		if custom_volume: audio_volume = custom_volume
		else: audio_volume = audio_info.volume
		#var audio_volume = custom_volume || audio_info.volume
		# if custom_volume: audio_volume = custom_volume
		# elsif audio_info.volume
		# var audio_volume = custom_volume or audio_info.volume
		if !playing: self.play()

		var polyphonic_stream_playback = self.get_stream_playback()
		
		var stream_int = polyphonic_stream_playback.play_stream(audio_info.stream, 0, audio_volume)
	else:
		printerr("no tag provided, cannot play sound")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
