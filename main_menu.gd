extends CanvasLayer

func _ready():
	GameManager.connect("game_started", _on_game_started)
	show()

func _on_game_started():
	hide()

func _on_start_pressed():
	print("button pressed")
	GameManager.start_game()
	#get_tree().change_scene_to_file("res://scenes/Game.tscn")


func _on_settings_pressed():
	pass # Replace with function body.


func _on_quit_pressed():
	get_tree().quit()
