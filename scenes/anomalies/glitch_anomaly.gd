class_name GlitchAnomaly
extends Anomaly

func _ready():
	super()

	only_activate_offscreen = false
	only_activate_onscreen = false
	
	anomaly_name = "GlitchAnomaly"

# There are no guardrails on whether they can be activated here.
# That's done in outside logic
func activate():
	super()
	$ObjectSprite/GlitchRect.visible = true

# There are no guardrails on whether they can be deactivated here.
# That's done in outside logic
func deactivate():
	super()
	$ObjectSprite/GlitchRect.visible = false
