extends CanvasLayer

func _ready():
	GameManager.connect("game_started", _on_game_started)
	
func _on_game_started():
	self.show()
